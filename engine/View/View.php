<?php
/**
 * Created by PhpStorm.
 * User: aluno
 * Date: 22/03/18
 * Time: 18:57
 */

namespace Engine\View;


class View
{
    private $template;

    private $view;

    private $variable;

    private $viewPath="view";

    private $templatePath="view/_templates";

    public function __construct()
    {

    }

    /**
     * @param mixed $template
     * @return View
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @param mixed $view
     * @return View
     */
    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }

    /**
     * @param mixed $view
     * @return View
     */
    public function setVariable($variable,$value=null)
    {
        if(is_array($variable)){
            foreach ($variable as $variable_name => $variable_value){
                $this->variable[$variable_name] = $variable_value;
            }
        }
        else{
            $this->variable[$variable]=$value;
        }
        return $this;
    }

    public function run(){
        foreach ($this->variable as $variable_name => $variable_value){
            $$variable_name=$variable_value;
        }
        $CONTEXT_CREATOR=$this->viewPath."/".$this->view.".php";
        require $this->templatePath."/".$this->template.".php";
    }
}