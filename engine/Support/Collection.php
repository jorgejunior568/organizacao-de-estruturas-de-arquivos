<?php
/**
 * Created by PhpStorm.
 * User: aluno
 * Date: 22/03/18
 * Time: 18:59
 */

namespace Engine\Support;


class Collection
{
    /**
     * @var array
     */
    private $array;

    /**
     * Collection constructor.
     * @param array $array
     */
    public function __construct(array $array)
    {

        $this->array = $array;
    }

    private static function cmp($field,$a,$b){
        return strcmp($a[$field],$b[$field]);
    }
    public function sort($field){
        usort($this->array,[self::class,"cmp"]);
    }
}