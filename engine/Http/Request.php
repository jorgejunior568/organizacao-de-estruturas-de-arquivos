<?php
/**
 * Created by PhpStorm.
 * User: aluno
 * Date: 22/03/18
 * Time: 20:08
 */

namespace Engine\Http;


class Request
{
    private $method;
    public function __construct()
    {
        $this->method=$_SERVER['REQUEST_METHOD'];
    }
    public function input($index,$default=null){
        $method="_".$this->method;
        eval("\$methodInfos=\$$method;");
        return $methodInfos[$index] ?? $default;
    }
    public function only($indexes,$default=null){
        $r=[];
        foreach ($indexes as $index){
            $r[$index]=$this->input($index,$default);
        }
        return $r;
    }
}