<?php
/**
 * Created by PhpStorm.
 * User: aluno
 * Date: 22/03/18
 * Time: 19:13
 */

namespace Controller;


use Engine\File\File;
use Engine\Http\Request;
use Engine\View\View;
use Exception\InvalidLineAddress;
use Model\Address;

class AddressController
{
    protected $viewEngine;

    public function __construct()
    {
        $this->viewEngine=new View();
    }

    public function index(){
//        $this->viewEngine
    }

    /**
     * @param File $file
     * @param $cep
     * @param $line_size
     * @param $min
     * @param $max
     * @return Address|false
     */
    public function binarySearch(File $file,$cep, $line_size, $min, $max){
        $middle=ceil(($max-$min)/2);
        /** @var array <Address> $lines */
        $lines=[
            "min" => $file->read($line_size,($line_size*$min)),
            "middle" => $file->read($line_size,($line_size*$middle)),
            "max" => $file->read($line_size,($line_size*$max))
        ];

        try {
            $lines=array_map("address_from_line", $lines);
            die(var_dump($lines));
        }catch (InvalidLineAddress $exception){
            die($exception->getMessage());
        }
        if($lines['min']->cep()==$cep){
            return $lines['min'];
        }
        elseif($lines['middle']->cep()==$cep){
            return $lines['middle'];
        }
        elseif($lines['max']->cep()==$cep){
            return $lines['max'];
        }
        elseif(($lines['max']-$lines['min'])<=1){
            return false;
        }
        elseif(
            (int) $lines['min']->cep()< (int) $cep &&
            (int) $lines['middle']->cep() > (int) $cep
        ){
            return $this->binarySearch($file,$cep,$lines['min']+1,$lines['middle']-1);
        }
        else{
            return $this->binarySearch($file,$cep,$lines['middle']+1,$lines['max']-1);

        }
    }

    public function search(){
        $request = new Request();
        $file_template=[
            72,
            72,
            72,
            72,
            2,
            "ID" => 8,
            2
        ];
        $cep=$request->input("cep");

        $file_path="data/cep.dat";
        $file_size=filesize($file_path);
        $line_size=array_sum($file_template);
        $file_lines=$file_size/$line_size;

        $search_opts=[
            "max" => $file_lines-1,
            "min" => 0
        ];

        $file=new File();
        $file->open($file_path,"r");

        $address=$this->binarySearch($file,$cep,$line_size,$search_opts['min'],$search_opts['max']);
        $file->close();
        $this
            ->viewEngine
            ->setTemplate("app")
            ->setView("search")
            ->setVariable("address",$address)
            ->run();
    }
}