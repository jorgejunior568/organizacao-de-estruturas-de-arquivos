<?php
/**
 * Created by PhpStorm.
 * User: gpca
 * Date: 22/03/18
 * Time: 17:32
 */

namespace Model;


class Address
{
    /**
     * @var null
     */
    private $cidade;
    /**
     * @var null
     */
    private $bairro;
    /**
     * @var null
     */
    private $logradouro;
    /**
     * @var null
     */
    private $estado;
    /**
     * @var null
     */
    private $uf;
    /**
     * @var null
     */
    private $cep;

    /**
     * Address constructor.
     * @param string $cidade
     * @param string $bairro
     * @param string $logradouro
     * @param string $estado
     * @param string $uf
     * @param string $cep
     */
    public function __construct($cidade=null, $bairro=null, $logradouro=null, $estado=null, $uf=null, $cep=null)
    {

        $this->cidade = $cidade;
        $this->bairro = $bairro;
        $this->logradouro = $logradouro;
        $this->estado = $estado;
        $this->uf = $uf;
        $this->cep = $cep;
    }

    /**
     * @param string|null $cidade
     * @return Address|string
     */
    public function cidade($cidade=null)
    {
        if(!is_null($cidade)) {
            $this->cidade = $cidade;
            return $this;
        }else return $this->cidade;
    }

    /**
     * @param string|null $bairro
     * @return Address|string
     */
    public function bairro($bairro=null)
    {
        if(!is_null($bairro)) {
            $this->bairro = $bairro;
            return $this;
        }else return $this->bairro;
    }

    /**
     * @param string|null $logradouro
     * @return Address|string
     */
    public function logradouro($logradouro=null)
    {
        if(!is_null($logradouro)) {
            $this->logradouro = $logradouro;
            return $this;
        }else return $this->logradouro;
    }
    /**
     * @param string|null $cidade
     * @return Address|string
     */
    public function estado($estado=null)
    {
        if(!is_null($estado)) {
            $this->estado = $estado;
            return $this;
        }else return $this->estado;
    }

    /**
     * @param string|null $bairro
     * @return Address|string
     */
    public function uf($uf=null)
    {
        if(!is_null($uf)) {
            $this->uf = $uf;
            return $this;
        }else return $this->uf;
    }

    /**
     * @param string|null $logradouro
     * @return Address|string
     */
    public function cep($cep=null)
    {
        if(!is_null($cep)) {
            $this->cep = $cep;
            return $this;
        }else return $this->cep;
    }
}