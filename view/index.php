<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>BUSCAR CEP</h2>
            <hr>
            <div class="row col-md-12">
                <form method="POST" class="form-horizontal">
                    <div class="form-group">
                        <label for="cep" class="control-label">CEP:</label>
                        <input type="text" class="form-control" name="cep" id="cep">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success pull-right">BUSCAR</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>