<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Resultado da busca</h2>
            <hr>
            <div class="row col-md-12">
                <?php if(!$address):?>
                    <h3 class="text-danger">
                        NENHUM RESULTADO ENCONTRADO
                    </h3>
                <?php else:?>
                    <blockquote>
                        <p>UF: <?= $address->uf();?></p>
                        <p>Cidade: <?= $address->cidade();?></p>
                        <p>Bairro: <?= $address->bairro();?></p>
                        <p>Logradouro: <?= $address->logradouro();?></p>
                        <p>Numero: <?= $address->numero();?></p>
                        <cite>CEP: <?= $address->cep();?></cite>
                    </blockquote>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>