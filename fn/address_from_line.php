<?php
use Exception\InvalidLineAddress;

/**
 * @return \Model\Address|null
 * @param $line string
 * @throws InvalidLineAddress
 */
function address_from_line($line){
    if(strlen($line)!=300) throw new InvalidLineAddress();
    else{
        $address=new \Model\Address();
        return $address
            ->logradouro(substr($line,0,72))
            ->bairro(substr($line,72,72))
            ->cidade(substr($line,144,72))
            ->estado(substr($line,216,72))
            ->uf(substr($line,288,2))
            ->cep(substr($line,290,8));

    }
}