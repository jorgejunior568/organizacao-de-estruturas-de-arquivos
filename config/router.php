<?php

use Engine\Router\Router;

$router = new Router();

$router
    ->get("index","/",[
        \Controller\AddressController::class,
        "search"
    ]);

$router->run();